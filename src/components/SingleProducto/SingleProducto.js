import React, { Component } from 'react'
import './SingleProducto.css'

import Error from '../Error/Error'

export default class SingleProducto extends Component {
  render() {
    if (!this.props.producto) return <Error mensaje="Producto no encontrado" />

    const { imagen, nombre, precio, descripcion } = this.props.producto

    return (
      <div className="info-producto">
        <div className="imagen">
          <img src={`../../assets/img/${imagen}.png`} alt={nombre} />
        </div>
        <div className="info">
          <h2>{nombre}</h2>
          <p className="precio">${precio}</p>
          <p>{descripcion}</p>
        </div>
      </div>
    )
  }
}
