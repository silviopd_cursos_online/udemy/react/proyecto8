import React, { Component } from 'react'
import './Contacto.css'

export default class Contacto extends Component {
  render() {
    return (
      <form>
        <legend>Formulario de contacto</legend>
        <div className="input-field">
          <label htmlFor="nombre">Nombre:</label>
          <input type="text" placeholder="tu nombre" id="nombre" />
        </div>
        <div className="input-field">
          <label htmlFor="email">Email:</label>
          <input type="email" placeholder="tu email" id="email" />
        </div>
        <div className="input-field">
          <label htmlFor="mensaje">Mensaje:</label>
          <textarea name="" id="mensaje" cols="30" rows="10" />
        </div>
        <div className="input-field enviar">
          <input type="submit" value="enviar" />
        </div>
      </form>
    )
  }
}
