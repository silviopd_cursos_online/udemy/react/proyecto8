import React, { Component } from 'react'
import './Nosotros.css'

export default class Nosotros extends Component {
  render() {
    return (
      <div className="contenedor-nosotros">
        <div className="imagen">
          <img src="../../../assets/img/camisa_1.png" alt="imagen nosotros" />
        </div>
        <div className="contenido">
          <h2>Sobre nosotros</h2>
          <p>
            Laborum eiusmod culpa aute ipsum commodo ex aliqua nostrud cillum.
            Et tempor Lorem id veniam amet Lorem magna dolor consectetur
            laborum. Dolore veniam duis nisi incididunt occaecat cillum do
            eiusmod anim veniam ad. Id veniam quis officia ut labore velit. Ut
            tempor dolor sunt est ullamco sit nisi consequat. Consequat deserunt
            voluptate eiusmod esse dolor cillum irure.
          </p>
        </div>
      </div>
    )
  }
}
