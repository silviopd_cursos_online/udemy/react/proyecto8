import React, { Component } from 'react'
import Logo from '../../../assets/img/logo.png'
import { Link } from 'react-router-dom'

export default class Header extends Component {
  render() {
    return (
      <div>
        <header>
          <Link to={'/'}>
            <img src={Logo} alt="logo imagen" />
          </Link>
        </header>
      </div>
    )
  }
}
